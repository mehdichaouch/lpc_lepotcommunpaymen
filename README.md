# Lpc_LePotCommunPaymen

Follow the procedure to install the extension in magento:

1. Copy the contents of folder "magento" inside your magento root directory.
2. Go to admin panel -> system -> cache management and flush all caches.
3. Logout of admin panel and login again.
4. Go to system -> configuration -> payment methods -> Le Pot Commun and specify your configuration settings.
5. After this your websie is ready to accept payments through lepotcommun.fr.


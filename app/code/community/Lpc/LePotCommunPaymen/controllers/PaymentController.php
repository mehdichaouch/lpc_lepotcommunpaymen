<?php

/**
 * ver. 1.0.0
 * Le Pot Commun Paymen
 *
 * @license  Copyright (c) 2015 Lakooz SAS - France
 *    
 * http://www.lepotcommun.fr
 */ 
class Lpc_LePotCommunPaymen_PaymentController extends Mage_Core_Controller_Front_Action {

    /**
     * @var Mage_Sales_Model_Order
     */
    protected $_order = null;

    /**
     * @return Mage_Checkout_Model_Session
     */
    public function getSession() {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * @return PayU_Account_Model_Payment
     */
    public function getPayment() {
        return Mage::getModel('lpc_lepotcommunpaymen/payment');
    }   
    
    /**
     * @return Lpc_LePotCommunPaymen_Model_Session
     */
    public function getLpcSession() {

        return Mage::getSingleton('lpc_lepotcommunpaymen/session');
    }
    
    /*
     * Notify action from the lpc gateway
     * 
     */
    public function notifyAction() {
        
        if ($this->getRequest()->isPost() ) {
            
            Mage::getSingleton('checkout/session')->unsQuoteId();
            $request = $this->getRequest();            
            $this->getPayment()->orderNotifyRequest($request); 
            
            
        } else
            Mage_Core_Controller_Varien_Action::_redirect('');
    }

    /**
     * @return Mage_Sales_Model_Order
     */
    public function getOrder() {

        if (is_null($this->_order)) {
            $this->_order = Mage::getModel('sales/order')
                    ->loadByIncrementId($this->getSession()->getLastRealOrderId());
        }
        return $this->_order;
    }

    /**
     * Check if the order is new
     * @return boolean
     */
    private function _isNewOrder() {
        return ($this->getSession()->getLastRealOrderId() == $this->getOrder()->getRealOrderId());
    }

    /* Forcing the order to be new */

    protected function _forceNewOrderStatus() {
        if ($this->_isNewOrder()) {
            $status = $this->getOrder()->getStatus();
            $state = $this->getOrder()->getState();
            $configStatus = Mage_Sales_Model_Order::STATE_PENDING_PAYMENT;
            if ($configStatus && $state == Mage_Sales_Model_Order::STATE_NEW && $status != $configStatus) {
                $this->getOrder()
                        ->setState($configStatus, true)
                        ->save();
            }
        }
    }

    /*
     * The new order action is triggered when an order is created
     * 
     */
    public function newAction() {
        try {
            $config = Mage::getModel('lpc_lepotcommunpaymen/config');
            $merchantId = $config->getMerchantId();
            $apiKey = $config->getMerchantKey();
            if ($merchantId == '' || $apiKey == '') {
                $this->_redirect('*/*/error');
                return;
            }
            
            $redirectData = $this->getPayment()->orderCreateRequest($this->getOrder());

            if (isset($redirectData['paymentPageUrl'])) {
                $this->_redirectUrl($redirectData['paymentPageUrl']);
                return;
            } else {
                $this->getSession()->addError(
                        $this->__('There was a problem with the payment initialization, please contact system administrator.')
                );
            }
        } catch (Mage_Core_Exception $e) {
            $this->getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->getSession()->addException($e, $this->__('There was a problem with the payment initialization, please contact system administrator.')
            );
        }
        $this->_redirect('*/*/error');
    }

    /**
     * payment success action
     */
    public function successAction() {

        if (TRUE) {
            try {
                $this->getSession()->getQuote()->setIsActive(false)->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }
            $this->_redirect('checkout/onepage/success', array('_secure' => true));
        } else {
            $this->norouteAction();
        }
        return;
    }    

    /**
     * Error payment action
     */
    public function errorAction() {
        $this->_redirect('checkout/onepage/failure', array('_secure' => true));
    }

    private function validateClient() {

        $host = Mage::helper('lpc_lepotcommunpaymen')->getDomainName();
        Mage::log($host, NULL, 'lpc.log');
        $expectedHost = "";
        $environment = Mage::getStoreConfig('payment/lpc_lepotcommunpaymen/domain', Mage::app()->getStore()->getId());
        if ($environment == 'testing') {
            $expectedHost = 'preprod.lepotcommuntest.fr';
        } else if ($environment == 'production') {
            $expectedHost = 'www.lepotcommun.fr';
        }
        if ($host == $expectedHost) {
            return TRUE;
        }
        return false;
    }

}

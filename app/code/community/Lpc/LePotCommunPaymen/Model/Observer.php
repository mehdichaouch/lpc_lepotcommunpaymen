<?php

/**
 * ver. 1.0.0
 * Le Pot Commun Paymen
 *
 * @license  Copyright (c) 2015 Lakooz SAS - France
 *    
 * http://www.lepotcommun.fr
 */ 
class Lpc_LePotCommunPaymen_Model_Observer
{    
    
    
    /*
     * observer for Controller_Action_Predispatch
     */   
    public function controllerActionBefore($observer){        
        
        $privateMode = $this->getConfig()->getIsPrivateModeEnabled();        
        if ($privateMode){            
            $request = $observer->getEvent()->getControllerAction()->getRequest();                        
            if ($lpcPayment = $request->getParam('lpc_payment')){                  
                $this->getLpcSession()->setLpcPayment($lpcPayment);          
                }            
        }        
    }
    
    /**
     * @return Lpc_LePotCommunPaymen_Model_Payment
     */
    public function getPayment() {
        return Mage::getModel('lpc_lepotcommunpaymen/payment');
    }
    
    /**
     * @return Lpc_LePotCommunPaymen_Model_Config
     */
    public function getConfig() {
        return Mage::getSingleton('lpc_lepotcommunpaymen/config');
    }
    
    /**
     * @return Lpc_LePotCommunPaymen_Model_Session
     */
    public function getLpcSession() {
        return Mage::getSingleton('lpc_lepotcommunpaymen/session');
    }
    
    
  
}
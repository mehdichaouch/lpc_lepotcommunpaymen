<?php

/**
 * ver. 1.0.0
 * Le Pot Commun Paymen
 *
 * @license  Copyright (c) 2015 Lakooz SAS - France
 *    
 * http://www.lepotcommun.fr
 */ 
class Lpc_LePotCommunPaymen_Model_System_Config_Source_Environment
{
    /**
     * @var string Production environment
     */
    const PRODUCTION = 'production';

    /**
     * @var string Testing environment
     */
    const SANDBOX = 'testing';
    
    /**
     * @var string Live environment
     */
    const LIVE = 'live';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'value' => self::PRODUCTION,
                'label' => Mage::helper('lpc_lepotcommunpaymen')->__('Production')
            ),
            array(
                'value' => self::SANDBOX,
                'label' => Mage::helper('lpc_lepotcommunpaymen')->__('Testing')
            ),
            array(
                'value' => self::LIVE,
                'label' => Mage::helper('lpc_lepotcommunpaymen')->__('Live')
            )
        );
    }
}

<?php

/**
 * ver. 1.0.0
 * Le Pot Commun Paymen
 *
 * @license  Copyright (c) 2015 Lakooz SAS - France
 *    
 * http://www.lepotcommun.fr
 */ 
class Lpc_LePotCommunPaymen_Model_Config
{
    /**
     * @var string self version
     */
    protected $_pluginVersion = '1.0.2';

    /**
     * @var string minimum Magento e-commerce version
     */
    protected $_minimumMageVersion = '1.6.0';

    /**
     * @var string latest version path in goods array
     */
    protected $_latestVersionPath = "plugins_magento_1.6.0";    

    /**
     * @var array latest plugin version info
     */
    protected $_latestVersion = array(
        'lang'        => 'en',
        'version'     => '1.0.0',
        'description' => 'This module allows customers to pay their orders with Le Pot Commun.fr',
        'logo'        => 'https://www.lepotcommun.fr/static/img/logo-cagnotte-en-ligne-le-pot-commun.png?h=326282399',
        'docs'        => array(            
            'websites' => array(
                0 => array(
                    'name' => 'Information site',
                    'url'  => 'http://www.lepotcommun.fr/',
                ),
            ),
        ),
    );

    

    /**
     * @var int
     */
    protected $_storeId;

    /**
     * Constructor
     *
     * @param $params
     */
    public function __construct($params = array())
    {
        // assign current store id
        $this->setStoreId(Mage::app()->getStore()->getId());
    }
    
    /**
     * @return Lpc_LePotCommunPaymen_Helper_Data
     */
    protected function _helper()
    {
        return Mage::helper('lpc_lepotcommunpaymen');
    }

    /**
     * @param int $storeId
     * @return $this
     */
    public function setStoreId($storeId)
    {
        $this->_storeId = $storeId;
        return $this;
    }

    /**
     * @return string get current environment
     */
    public function getEnvironment()
    {
        
        return $this->getStoreConfig('payment/lpc_lepotcommunpaymen/domain');
    }

    /** @return string get Merchant Id */
    public function getMerchantId()
    {
        return $this->getClientId();
    }

    /**
     * @return string get Merchant Key
     */
    public function getMerchantKey()
    {
        return $this->getStoreConfig('payment/lpc_lepotcommunpaymen/merchant_key');
    }

    /**
     * @return string get Merchant Id
     */
    public function getClientId()
    {
        return $this->getStoreConfig('payment/lpc_lepotcommunpaymen/merchant_id');
    }
    
    /**
     * @return bool getIsPrivateModeEnabled
     */
    public function getIsPrivateModeEnabled(){        
        return $this->getStoreConfig('payment/lpc_lepotcommunpaymen/private_mode_enabled');        
    }

    /**
     * @return string base module url
     */
    public function getUrl($action)
    {
        return Mage::getUrl("lpcpaymen/payment/$action", array('_secure' => true));
    }    

    /**
     * @return bool
     */
    public function isLatestVersionInstalled()
    {
        return !Mage::helper('lpc_lepotcommunpaymen')->isFirstVersionNewer($this->_latestVersion['version'], $this->_pluginVersion);
    }

    /**
     * @return string advertisement banner url
     */
    public function getAdvertisementSrc()
    {
        return $this->_latestVersion['logo'];
    }

    /**
     * @return array get latest plugin version data
     */
    public function getLatestVersion()
    {
        return $this->_latestVersion;
    }

    

    /**
     * @return string get current plugin version
     */
    public function getPluginVersion()
    {
        return $this->_pluginVersion;
    }

    /**
     * @return string get minimum mage version for the plugin to work on
     */
    public function getMinimumMageVersion()
    {
        return $this->_minimumMageVersion;
    }

    /**
     * Change locale of given string
     *
     * @param $string
     * @param $s
     * @return string
     */
    protected function localize($string, $s = "/")
    {
        return Mage::helper('lpc_lepotcommunpaymen')->localize($string, $s);
    }

    /**
     * get Store Config variable
     * @param $name
     * @return string
     */
    protected function getStoreConfig($name)
    {
        return Mage::getStoreConfig($name, $this->_storeId);
    }

    
}

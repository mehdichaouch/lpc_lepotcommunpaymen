<?php

/**
 * ver. 1.0.0
 * Le Pot Commun Paymen
 *
 * @license  Copyright (c) 2015 Lakooz SAS - France
 *    
 * http://www.lepotcommun.fr
 */  
class Lpc_LePotCommunPaymen_Model_Payment extends Mage_Payment_Model_Method_Abstract {

    /**
     * Payment method code
     * @var string
     */
    protected $_code = 'lpc_lepotcommunpaymen';

    /**
     * Block type
     * @var string
     */
    protected $_formBlockType = 'lpc_lepotcommunpaymen/form';

    /**
     * Is initialization needed
     * @var boolean
     */
    protected $_isInitializeNeeded = true;

    /**
     * Transaction id
     */
    protected $_transactionId;

    /**
     * Pay method
     */
    protected $_lpcPayMethod;

    /**
     * Currently processed order
     * @var Mage_Sales_Model_Order
     */
    protected $_order;
    protected $_tempInfo = "AWAITING_Kitty";
    protected $_isGateway = true;
    protected $_canOrder = true;
    protected $_canAuthorize = true;
    protected $_canCapture = true;
    protected $_canCapturePartial = false;
    protected $_canRefund = true;
    protected $_canRefundInvoicePartial = true;
    protected $_canVoid = false;
    protected $_canUseInternal = false;
    protected $_canUseCheckout = true;
    protected $_canUseForMultishipping = false;
    protected $_canSaveCc = false;
    protected $_canReviewPayment = true;
    protected $_lpcOrderResult = null;

    public function __construct() {

        parent::__construct();
        $this->initializeLpcConfiguration();
    }

    /**
     * @return Lpc_LePotCommunPaymen_Helper_Data
     */
    protected function _helper() {

        return Mage::helper('lpc_lepotcommunpaymen');
    }

    /**
     * @return Lpc_LePotCommunPaymen_Model_Session
     */
    public function getLpcSession() {

        return Mage::getSingleton('lpc_lepotcommunpaymen/session');
    }

    /**
     * Initialize Lpc configuration
     */
    protected function initializeLpcConfiguration() {

        $returnUrls = array();
        $returnUrls ['okUrl'] = $this->getConfig()->getUrl('success');
        $returnUrls ['koUrl'] = $this->getConfig()->getUrl('error');
        $returnUrls ['notificationUrl'] = $this->getConfig()->getUrl('notify');

        LPC_Configuration::setApiVersion(1.0);
        $merchantId = $this->getConfig()->getMerchantId();
        Lpc_Configuration::setEnvironment($this->getConfig()->getEnvironment(), $returnUrls);
        LPC_Configuration::setMerchantId($merchantId);
        LPC_Configuration::setApiKey($this->getConfig()->getMerchantKey());
    }

    /**
     * Get Lpc configuration
     * @return Lpc_LePotCommunPaymen_Model_Config
     */
    public function getConfig() {

        return Mage::getSingleton('lpc_lepotcommunpaymen/config');
    }

    /**
     * @param int $extOrderId
     * @return $this
     */
    public function setOrderByOrderId($extOrderId) {

        $this->_order = Mage::getModel('sales/order')->load($extOrderId);
        return $this;
    }

    /**
     * @return Mage_Sales_Model_Order
     */
    public function getOrder() {

        return $this->_order;
    }

    /*
     * override the default availability check for the payment method
     */
    public function isAvailable($quote = null) {

        $initialCheck = parent::isAvailable();
        if ($initialCheck) {
            if ($this->getConfig()->getIsPrivateModeEnabled()) {
                if ($lpcPayment = $this->getLpcSession()->getLpcPayment()) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } else {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @return Mage_Sales_Model_Order
     */
    public function setOrder(Mage_Sales_Model_Order $order) {

        $this->_order = $order;
        return $this;
    }

    /**
     * Get Lpc session namespace
     * @return Lpc_LePotCommunPaymen_Model_Session
     */
    public function getSession() {

        return Mage::getSingleton(($this->_helper()->getNameSpaceModule()) . 'lpc_lepotcommunpaymen/session');
    }

    /**
     * Redirection url
     * @return string
     */
    public function getOrderPlaceRedirectUrl() {

        return Mage::getUrl($this->_helper()->getFrontName() . '/payment/new', array('_secure' => true));
    }

    /**
     * Get current quote
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote() {

        return $this->getCheckoutSession()->getQuote();
    }

    /**
     * Get checkout session namespace
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckoutSession() {

        return Mage::getSingleton('checkout/session');
    }

    /**
     * Initializes the payment.
     * @param Mage_Sales_Model_Order
     * @return array
     */
    public function orderCreateRequest(Mage_Sales_Model_Order $order) {
        
        $this->setOrder($order);
        $orderCurrencyCode = $order->getOrderCurrencyCode();
        $orderCountryCode = $order->getBillingAddress()->getCountry();
        $grandTotal = $order->getGrandTotal();

        $OCReq = array();
        $OCReq ['currency'] = $orderCurrencyCode;
        $OCReq['merchantId'] = LPC_Configuration::getMerchantId();
        $OCReq ['transactionId'] = $order->getRealOrderId();
        $OCReq ['amount'] = $this->_helper()->toAmount($grandTotal);

        try {
            
            $result = LPC_Order::create($OCReq);

            if ($result != NULL) {

                // store session identifier in session info
                Mage::getSingleton('core/session')->setLpcSessionId($this->getCheckoutSession()->getLastOrderId());

                // assign current transaction id
                $this->_transactionId = $result['lpcTransactionId'];
                $order->getPayment()->setLastTransId($this->_transactionId);
				$order->getPayment()->setAdditionalInformation('lpc_original_transaction_id', $this->_transactionId);
                $order->getPayment()->save();                

                $customer = Mage::getModel('customer/customer');
                if ($order->getCustomerIsGuest()) {
                    $billingAddressId = $order->getBillingAddressId();
                    if (!empty($billingAddressId)) {
                        $billingAddress = $order->getBillingAddress();
                        $email = $billingAddress->getEmail();
                        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
                        $customer->loadByEmail($email);
                    }


                    if (!$customer->getId()) {

                        $order->setCustomerEmail($email);
                    }
                } else {

                    $customer->load($order->getCustomerId());
                }
            } else {

                Mage::throwException($this->_helper()
                                ->__('There was a problem with the payment initialization, please contact system administrator.'));
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }

        //$order->sendNewOrderEmail();
        //$order->setEmailSent(true);
        $order->save();

        return $result;
    }

    /**
     * Refund payment
     * @param Varien_Object $payment
     * @param float $amount
     * @return $this
     */
    public function refund(Varien_Object $payment, $amount) {        

        $order = $payment->getOrder();

        $amount = $this->_toAmount($amount);
        $orderCurrencyCode = $order->getOrderCurrencyCode();

        $OCReq = array();
        $OCReq ['currency'] = $orderCurrencyCode;
        $OCReq ['merchantId'] = LPC_Configuration::getMerchantId();
        $OCReq ['transactionId'] = $order->getRealOrderId()."_".LPC_Util::randomString(8)."_refund";
        $OCReq ['originalLPCTransactionId'] = $order->getPayment()->getLastTransId();
        $OCReq ['amount'] = $amount;
		
		error_log($OCReq ['transactionId']);
		error_log($OCReq ['originalLPCTransactionId']);

        try {

            $result = LPC_Order::cancel($OCReq);

            $payment->setLastTransId($result['lpcTransactionId'])->save();
        } catch (Exception $e) {

            Mage::throwException($e->getMessage());
        }

        return $this;
    }

    /**
     * Returns amount in LPC acceptable format
     *
     * @param $val
     * @return int
     */
    protected function _toAmount($val)
    {
        return $this->_helper()->toAmount($val);
    }
    
    
    /**
     * Create invoice
     * @return Mage_Sales_Model_Order_Invoice
     */
    protected function _initInvoice() {
        
        $items = array();
        foreach ($this->getOrder()->getAllItems() as $item) {

            $items[$item->getId()] = $item->getQtyOrdered();
        }

        /* @var $invoice Mage_Sales_Model_Service_Order */
        $invoice = Mage::getModel('sales/service_order', $this->getOrder())->prepareInvoice($items);
        $invoice->setEmailSent(true)->register();
        Mage::register('current_invoice', $invoice);

        return $invoice;
    }    

    /*
     * function order notify request recieves the notification from payment validation page
     * @param $request
     */
    public function orderNotifyRequest($request) {        

        try {

            $this->_transactionId = $request->getPost('lpcTransactionId');
            $orderId = $request->getPost('transactionId');
            
            $transactionType = $request->getPost('type');

            $status = $request->getPost('status');

            $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
            $this->setOrder($order);
			
			$originalLpcTransactionId = $this->getOrder()->getPayment()->getLastTransId();
			
			
            if (isset($orderId) && isset($originalLpcTransactionId) && strcmp(trim ($originalLpcTransactionId),trim($this->_transactionId)) == 0 && $transactionType == 'CREATE') {
			
				$OCReq = array();
				$OCReq ['merchantId'] = LPC_Configuration::getMerchantId();
				$OCReq ['transactionId'] = $orderId;
				$OCReq ['lpcTransactionId'] = $originalLpcTransactionId;
				$result = LPC_Order::retrieve($OCReq);
				
				
				if(!empty($result) && $result['type'] == 0){
					$statusInt = $result['status'];
					if ($statusInt == 1) {                    
                    $this->_updatePaymentStatus(LPC_Order::STATUS_SUCCESS);
					} else if ($statusInt == 2) {
						$this->_updatePaymentStatus(LPC_Order::STATUS_REJECTED);                
					} else if ($statusInt == 0) {
						$this->_updatePaymentStatus(LPC_Order::STATUS_NEW);
					}
				}
				else{
					error_log('empty response from lpc get order : '.$this->_transactionId .' lpcTransactionID : '.$originalLpcTransactionId .' transaction type : '.$transactionType);
				}
            }
			else {
				error_log('External call with an invalid data lpcTransactionID : '.$this->_transactionId .' lpcTransactionID : '.$originalLpcTransactionId .' transaction type : '.$transactionType);
			}

            //the response should be status 200

            header("HTTP/1.1 200 OK");

            exit();
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * Update payment status
     * @param $paymentStatus
     */
    protected function _updatePaymentStatus($paymentStatus) {        

        $payment = $this->getOrder()->getPayment();

        $currentState = $payment->getAdditionalInformation('lpc_payment_status');
        if ($currentState != LPC_Order::STATUS_COMPLETED && $currentState != $paymentStatus) {

            try {

                switch ($paymentStatus) {

                    case LPC_Order::STATUS_NEW:

                        $this->_updatePaymentStatusNew($payment);

                        break;



                    case LPC_Order::STATUS_PENDING:

                        $this->_updatePaymentStatusPending($payment);

                        break;



                    case LPC_Order::STATUS_CANCELED:

                        $this->_updatePaymentStatusCanceled($payment);

                        break;



                    case LPC_Order::STATUS_REJECTED:

                        $this->_updatePaymentStatusDenied($payment);

                        break;



                    case LPC_Order::STATUS_SUCCESS:

                        $payment->setAdditionalInformation('lpc_original_transaction_id', $this->_transactionId)->save();

                        $this->_updatePaymentStatusCompleted($payment);

                        break;
                }                

                $payment->setAdditionalInformation('lpc_payment_status', $paymentStatus)->save();
            } catch (Exception $e) {

                Mage::logException($e);
            }
        }
    }

    /**
     * Update payment status to new
     * @param Mage_Sales_Model_Order_Payment $payment
     */
    protected function _updatePaymentStatusNew(Mage_Sales_Model_Order_Payment $payment) {

        $comment = $this->_helper()->__('New transaction started.');



        $payment->setTransactionId($this->_transactionId)
                ->setPreparedMessage($comment)
                ->setIsTransactionClosed(false)
                ->save();



        $payment->addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_ORDER)
                ->save();



        $payment->getOrder()
                ->save();



        $payment->getOrder()->addStatusHistoryComment($comment)
                ->save();
    }

    /**
     * Update payment status to pending
     * @param Mage_Sales_Model_Order_Payment $payment
     */
    protected function _updatePaymentStatusPending(Mage_Sales_Model_Order_Payment $payment) {

        $comment = $this->_helper()->__('The transaction is pending.');



        $payment->setTransactionId($this->_transactionId)
                ->setPreparedMessage($comment)
                ->setIsTransactionApproved(false)
                ->setIsTransactionClosed(false)
                ->save();



        $payment->addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_ORDER)
                ->save();



        $payment->getOrder()
                ->save();



        $payment->getOrder()->addStatusHistoryComment($comment)
                ->save();
    }

    /**
     * Change the status to canceled
     * @param Mage_Sales_Model_Order_Payment $payment
     */
    protected function _updatePaymentStatusCanceled(Mage_Sales_Model_Order_Payment $payment) {

        $comment = $this->_helper()->__('The transaction has been canceled.');



        $payment->setTransactionId($this->_transactionId)
                ->setPreparedMessage($comment)
                ->setIsTransactionApproved(false)
                ->setIsTransactionClosed(true)
                ->cancel()
                ->save();



        $payment->addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_ORDER)
                ->save();





        $payment->getOrder()
                ->sendOrderUpdateEmail(true, $comment)
                ->save();



        $payment->getOrder()->addStatusHistoryComment($comment)
                ->save();
    }

    /**
     * Change the status to rejected
     * @param Mage_Sales_Model_Order_Payment $payment
     */
    protected function _updatePaymentStatusDenied(Mage_Sales_Model_Order_Payment $payment) {

        $comment = $this->_helper()->__('The transaction has been rejected.');



        $payment->setTransactionId($this->_transactionId)
                ->setPreparedMessage($comment)
                ->setIsTransactionApproved(false)
                ->setIsTransactionClosed(true)
                ->deny()
                ->save();

        Mage::log('inside update order status denied', NULL, 'lpc.log');



        $payment->addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_ORDER)
                ->save();



        $payment->getOrder()
                ->sendOrderUpdateEmail(true, $comment)
                ->save();



        $payment->getOrder()->addStatusHistoryComment($comment)
                ->save();
    }

    /**
     * Update payment status to complete
     * @param Mage_Sales_Model_Order_Payment $payment
     */
    protected function _updatePaymentStatusCompleted(Mage_Sales_Model_Order_Payment $payment) {

        $comment = $this->_helper()->__('The transaction completed successfully.');


        $this->createInvoice($this->getOrder());

        $payment->setTransactionId($this->_transactionId)
                ->setPreparedMessage($comment)
                ->setCurrencyCode($payment->getOrder()->getBaseCurrencyCode())
                ->setIsTransactionApproved(true)
                ->setIsTransactionClosed(true)
                ->registerCaptureNotification($this->getOrder()->getTotalDue())
                ->save();

        $this->getOrder()
                ->save();

        // notify customer
        if ($invoice = $payment->getCreatedInvoice()) {

            $comment = $this->_helper()->__('Notified customer about invoice #%s.', $invoice->getIncrementId());

            if (!$this->getOrder()->getEmailSent()) {

                $this->getOrder()
                        ->queueNewOrderEmail()
                        ->setIsCustomerNotified(true)
                        ->addStatusHistoryComment($comment)
                        ->save();
            } else {

                $this->getOrder()
                        ->sendOrderUpdateEmail(true, $comment)
                        ->addStatusHistoryComment($comment)
                        ->save();

            }
        }
    }

    public function createInvoice($order) {

        if ($order->canInvoice()) {

            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
            $invoice->setTransactionId($this->_transactionId);
            $invoice->register()->pay();

            Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder())
                    ->save();

            $invoice->sendEmail();
            return $invoice;
        }
    }

}
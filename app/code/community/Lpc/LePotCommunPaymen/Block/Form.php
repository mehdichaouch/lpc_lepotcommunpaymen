<?php

/**
 * ver. 1.0.0
 * Le Pot Commun Paymen
 *
 * @license  Copyright (c) 2015 Lakooz SAS - France
 *    
 * http://www.lepotcommun.fr
 */  
class Lpc_LePotCommunPaymen_Block_Form extends Mage_Payment_Block_Form
{
    /**
     * Payment method code
     * @var string
     */
    protected $_methodCode = 'lpc_lepotcommunpaymen';
    
    /**
     * Set template and redirect message
     */
    protected function _construct()
    {
        $this->setTemplate('lpc_lepotcommunpaymen/form.phtml');

        return parent::_construct();
    }
    
    /**
     * Payment method code getter
     * @return string
     */
    public function getMethodCode()
    {
        return $this->_methodCode;
    }  
}
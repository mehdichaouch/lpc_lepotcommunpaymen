<?php

/**
 * ver. 1.0.0
 * Le Pot Commun Paymen
 *
 * @license  Copyright (c) 2015 Lakooz SAS - France
 *    
 * http://www.lepotcommun.fr
 */  
class Lpc_LePotCommunPaymen_Block_UpdateInfo extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        /** @var array Get latest version of the plugin */
        $latestVersion = $this->getConfig()->getLatestVersion();

        

        if (!$this->getConfig()->isLatestVersionInstalled()) {            
            $html .= $this->getConfig()->_helper()->__('You are currently using version') . ":<br><b>";
            $html .= $this->getConfig() . " " . $this->getConfig()->_helper()->__('for') . " magento " . $this->getConfig()->getMinimumMageVersion() . "+</b><hr>";

            $html .= Mage::helper('lpc_lepotcommunpaymen')->__('The latest version of Le Pot Commun extension is');
            $html .= " <b>" . $latestVersion['version'] . "</b>";            
        } else {
            $html .= Mage::helper('lpc_lepotcommunpaymen')->__('You are currently using version') . ":<br><b>";
            $html .= $this->getConfig()->getPluginVersion() . " " . Mage::helper('lpc_lepotcommunpaymen')->__('for') . " magento " . $this->getConfig()->getMinimumMageVersion() . "+</b><hr>";
        }
       
        if (count($latestVersion['docs']['websites']) > 0) {
            $html .= Mage::helper('lpc_lepotcommunpaymen')->__('More info on') . ":<br>";
        }

        if (isset($latestVersion['docs']['websites'])) {
            foreach ($latestVersion['docs']['websites'] as $key => $website) {
                $html .= $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setType('button')
                    ->setClass('scalable')
                    ->setLabel(Mage::helper('lpc_lepotcommunpaymen')->__($website['name']))
                    ->setOnClick("window.open('" . $website['url'] . "')")
                    ->toHtml();
                $html .= "<br>";
            }
        }

        $html .= Mage::helper('lpc_lepotcommunpaymen')->__($latestVersion['description']);
        return $html;
    }
    
    public function getConfig(){
        
        return Mage::getModel('lpc_lepotcommunpaymen/config');
        
    }
    
    
}
<?php

/**
 * ver. 1.0.0
 * Le Pot Commun Paymen
 *
 * @license  Copyright (c) 2015 Lakooz SAS - France
 *    
 * http://www.lepotcommun.fr
 */ 
class Lpc_LePotCommunPaymen_Block_Advertisement extends Mage_Core_Block_Template
{
    /**
     * Redirection after clicking the advertisement
     *
     * @var string
     */
    protected $_redirectUrl = 'http://www.lepotcommun.fr';

    /**
     * (non-PHPdoc)
     * @see magento/app/code/core/Mage/Core/Block/Mage_Core_Block_Template::_toHtml()
     */
    protected function _toHtml()
    {
        /**
         * setting the advertisement source url
         */
        $this->setAdvertisementSrc(Mage::getModel('lpc_lepotcommunpaymen/config')->getAdvertisementSrc());

        /**
         * setting the redirect url
         */
        $this->setRedirectUrl($this->_redirectUrl);

        return parent::_toHtml();
    }
}

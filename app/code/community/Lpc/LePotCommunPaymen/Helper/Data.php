<?php

/**
 * ver. 1.0.0
 * Le Pot Commun Paymen
 *
 * @license  Copyright (c) 2015 Lakooz SAS - France
 *    
 * http://www.lepotcommun.fr
 */   
class Lpc_LePotCommunPaymen_Helper_Data extends Mage_Core_Helper_Abstract
{      

    protected $frontname = "lpcpaymen";   

    protected $namespace = "Lpc";        

    protected $moduleName = "LePotCommunPaymen";    

     /**
     * Converts the Magento float values (for instance 9.9900) to Le Pot Commun accepted Currency format (999)
     * @param  string
     * @return int
     */
    public function toAmount($val)
    {
        $multiplied = $val * 100;
        $round      = (int)round($multiplied);
        return $round;
    }



    /**
     * Change locale of given string and separator
     * @param string $string
     * @param string $s
     * @return string
     */

    public function localize($string, $s = "/")
    {
        $lang = explode("_", Mage::app()->getLocale()->getLocaleCode());
        if (!in_array($lang[0], array("fr", "en"))){
            $lang[0] = "en";
        }
        
        $from = array("/pl/", "/en/", $s . "pl" . $s, $s . "en" . $s, "_en.", "_pl.");
        $to   = array("/" . $lang[0] . "/", "/" . $lang[0] . "/", $s . $lang[0] . $s, $s . $lang[0] . $s, "_" . $lang[0] . ".", "_" . $lang[0] . ".");

        return str_replace($from, $to, $string);
    }



    /**
     * @param $build1
     * @param $build2
     * @return bool
     */

    public function isFirstVersionNewer($build1, $build2)
    {
        $arr1 = explode(".", $build1);
        $arr2 = explode(".", $build2);
        $b1 = "";
        $b2 = "";

        foreach ($arr1 as $key => $b) {
            $b1 .= str_pad($arr1[$key], 4, "0", STR_PAD_LEFT);
            $b2 .= str_pad($arr2[$key], 4, "0", STR_PAD_LEFT);
        }
        return ((int)$b1 > (int)$b2);
    }

    

    /**
     * @return mixed
     */
    public function getDomainName()
    {
        return $_SERVER['HTTP_HOST'];
    }
    
    /**
     * @return string
     */
    public function getNameSpace()
    {
        return $this->namespace;
    }

    /**
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * @return string
     */
    public function getNameSpaceModule()
    {
        return $this->namespace . "_" . $this->moduleName;
    }

    /**
     * @return string
     */
    public function getFrontName()
    {
        return $this->frontname;
    }    

}